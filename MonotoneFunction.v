Require Import Utf8.
Require Import Setoid.
Require Import Lattice.

Record MonotoneFunction (X Y : Type)
    {LCX : LatticeCore X} {LCY : LatticeCore Y} : Type :=
  { mf_apply    : X → Y
  ; mf_monotone : ∀ x y : X, x ⊑ y → mf_apply x ⊑ mf_apply y
  }.
Notation "[ X ⇒ Y ]" := (MonotoneFunction X Y).

Coercion mf_apply : MonotoneFunction >-> Funclass.

Arguments mf_monotone {X Y LCX LCY}.

Add Parametric Morphism (X Y : Type)
  (LCX : LatticeCore X) (LCY : LatticeCore Y) 
  (f : [X ⇒ Y]) : (mf_apply X Y f) with
  signature lattice_le ++> lattice_le
  as MonotoneFunction_mor.
Proof. apply mf_monotone. Qed.

Section MonotoneFunction.
Context {X Y : Type}.
Context {LCX : LatticeCore X} {LCY : LatticeCore Y}.
Context {LX  : Lattice X} {LY : Lattice Y}.

Lemma mf_sup_monotone (F : [X ⇒ Y] → Prop) (x y : X) : x ⊑ y → 
  (∐ z, ∃ f : [X ⇒ Y], F f ∧ z = f x) ⊑ (∐ z, ∃ f : [X ⇒ Y], F f ∧ z = f y).
Proof.
intro Hxy; apply lattice_sup_min.
intros z [ f [ Hf Heq ] ]; subst.
rewrite Hxy.
apply lattice_sup_upper; eauto.
Qed.

Lemma mf_inf_monotone (F : [X ⇒ Y] → Prop) (x y : X) : x ⊑ y → 
  (∏ z, ∃ f : [X ⇒ Y], F f ∧ z = f x) ⊑ (∏ z, ∃ f : [X ⇒ Y], F f ∧ z = f y).
Proof.
intro Hxy; apply lattice_inf_max.
intros z [ f [ Hf Heq ] ]; subst.
rewrite <- Hxy.
apply lattice_inf_lower; eauto.
Qed.

Global Instance LatticeCore_MonotoneFunction : LatticeCore [X ⇒ Y] :=
  { lattice_le   := λ f g, ∀ x, f x ⊑ g x
  ; lattice_sup  := λ F,
      {| mf_apply    := λ x, ∐ y, ∃ f, F f ∧ y = f x
      ;  mf_monotone := mf_sup_monotone F
      |}
  ; lattice_inf  := λ F,
      {| mf_apply    := λ x, ∏ y, ∃ f, F f ∧ y = f x
      ;  mf_monotone := mf_inf_monotone F
      |}
  }.

Global Instance Lattice_MonotoneFunction : Lattice [X ⇒ Y].
Proof.
split; simpl.
+ firstorder.
+ firstorder.
+ intros; apply lattice_sup_upper; eauto.
+ intros F f Hf x.
  apply lattice_sup_min.
  intros y [ g [ Hg Heq ] ]; subst; eauto.
+ intros; apply lattice_inf_lower; eauto.
+ intros F f Hf x.
  apply lattice_inf_max.
  intros y [ g [ Hg Heq ] ]; subst; eauto.
Qed.

Context {Z : Type}.
Context {LCZ : LatticeCore Z} {LZ  : Lattice Z}.

Definition mf_id : [X ⇒ X] :=
  {| mf_apply    := λ x, x
  ;  mf_monotone := λ x y H, H
  |}.

Lemma mf_comp_monotone (f : [Y ⇒ Z]) (g : [X ⇒ Y]) x y : x ⊑ y →
  f (g x) ⊑ f (g y).
Proof.
intro Hxy; apply mf_monotone, mf_monotone; assumption.
Qed.

Definition mf_comp (f : [Y ⇒ Z]) (g : [X ⇒ Y]) : [X ⇒ Z] :=
  {| mf_apply    := λ x, f (g x)
  ;  mf_monotone := (mf_comp_monotone f g)
  |}.

Definition mf_const (y : Y) :=
  {| mf_apply    := λ x : X, y
  ;  mf_monotone := λ _ _ _, lattice_le_refl y
  |}.

End MonotoneFunction.

Notation "f ∘ g" := (mf_comp f g) (at level 50).

Add Parametric Morphism (X Y : Type)
  (LCX : LatticeCore X)
  (LCY : LatticeCore Y) (LY : Lattice Y) :
  (@mf_apply X Y LCX LCY) with
  signature lattice_le ++> lattice_le ++> lattice_le
  as mf_apply_mor.
Proof.
intros f g Hfg x y Hxy.
rewrite Hxy; apply Hfg.
Qed.

Add Parametric Morphism (X Y Z : Type)
  (LCX : LatticeCore X)
  (LCY : LatticeCore Y) (LY : Lattice Y)
  (LCZ : LatticeCore Z) (LZ : Lattice Z) :
  (@mf_comp X Y LCX LCY Z LCZ) with
  signature lattice_le ++> lattice_le ++> lattice_le
  as mf_comp_mor.
Proof.
intros f f' Hf g g' Hg x; simpl.
rewrite Hf; apply mf_monotone, Hg.
Qed.