# Diacritical Companions

This is the Coq formalization that accompanies the paper _Diacritical Companions_.

## Compilation

To compile the development, just type "make" in the main directory.
The development was tested under Coq version 8.8.2.

## Structure of the development

Here we briefly describe how Coq files map into sections of the paper.

### Basic notions

`Lattice.v` contains the definition of a complete lattice, `MonotoneFunctions.v`
defines monotone functions and lattice of monotone functions. Product of
lattices is defined in `LatticeProduct.v`. Finally, the definition of progress
and similarity is given in `Progress.v`.

### First-order definitions (Section 3)

File `Evolution.v` contains the definition of evolution and restricted
evolution, and proves their basic properties about composition.

The diacritical companion is defined in `Companion.v`. This file contains
its first-order properties, in particular Theorem 3.11 (`soundness`).
At the end of the file there is also Proposition 4.2 (`ucompan_below_wcompan`).

### Higher-order definitions (Section 4)

File `HigherOrder.v` contains the definition of higher-order companion,
as well as the proof of all statements of Proposition 4.5.

### Composition on the right (Section 4.2)

Results of Section 4.2 are formalized in `MakeUpToImproved.v`.
The last theorem `make_upto_better` corresponds to Theorem 4.12.
