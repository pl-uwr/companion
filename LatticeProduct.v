Require Import Utf8.
Require Import Setoid.
Require Import Lattice MonotoneFunction.

Section Product.
Context {X Y : Type}.
Context {LCX : LatticeCore X} {LCY : LatticeCore Y}.
Context {LX  : Lattice X} {LY : Lattice Y}.

Global Instance LatticeCore_Product : LatticeCore (X * Y) :=
  { lattice_le  := λ p q, fst p ⊑ fst q ∧ snd p ⊑ snd q
  ; lattice_sup := λ A, ((∐ x, ∃ y, A (x, y)), (∐ y, ∃ x, A (x, y)))
  ; lattice_inf := λ A, ((∏ x, ∃ y, A (x, y)), (∏ y, ∃ x, A (x, y)))
  }.

Global Instance Lattice_Product : Lattice (X * Y).
Proof.
split; simpl.
+ firstorder.
+ firstorder.
+ intros A [x y] Hxy; simpl.
  split; apply lattice_sup_upper; firstorder.
+ intros A [x y] HA; simpl.
  split; apply lattice_sup_min; firstorder.
+ intros A [x y] Hxy; simpl.
  split; apply lattice_inf_lower; firstorder.
+ intros A [x y] HA; simpl.
  split; apply lattice_inf_max; firstorder.
Qed.

Lemma mf_fst_monotone (p q : X * Y) : p ⊑ q → fst p ⊑ fst q.
Proof. firstorder. Qed.

Lemma mf_snd_monotone (p q : X * Y) : p ⊑ q → snd p ⊑ snd q.
Proof. firstorder. Qed.

Definition mf_fst : [X * Y ⇒ X] :=
  {| mf_apply    := fst
  ;  mf_monotone := mf_fst_monotone
  |}.
Definition mf_snd : [X * Y ⇒ Y] :=
  {| mf_apply    := snd
  ;  mf_monotone := mf_snd_monotone
  |}.

End Product.

Add Parametric Morphism (X Y : Type)
  (LCX : LatticeCore X) (LCY : LatticeCore Y) : (@fst X Y) with
  signature lattice_le ++> lattice_le
  as fst_mor.
Proof. firstorder. Qed.

Add Parametric Morphism (X Y : Type)
  (LCX : LatticeCore X) (LCY : LatticeCore Y) : (@snd X Y) with
  signature lattice_le ++> lattice_le
  as snd_mor.
Proof. firstorder. Qed.