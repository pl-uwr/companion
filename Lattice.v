Require Import Utf8.
Require Import Setoid.

Class LatticeCore (L : Type) : Type :=
  { lattice_le  : relation L
  ; lattice_sup : (L → Prop) → L
  ; lattice_inf : (L → Prop) → L
  }.

Notation "R ⊑ S" := (@lattice_le _ _ R S) (at level 70).
Notation "∐ R , P" := (lattice_sup (λ R, P)) 
  (at level 200, right associativity).
Notation "∏ R , P" := (lattice_inf (λ R, P))
  (at level 200, right associativity).

Class Lattice (L : Type) {LC : LatticeCore L} : Prop :=
  { lattice_le_refl   : reflexive L lattice_le
  ; lattice_le_trans  : transitive L lattice_le
  ; lattice_sup_upper : ∀ (A : L → Prop) R, A R → R ⊑ ∐ S, A S
  ; lattice_sup_min   : ∀ (A : L → Prop) R, (∀ S, A S → S ⊑ R) → (∐ S, A S) ⊑ R
  ; lattice_inf_lower : ∀ (A : L → Prop) R, A R → (∏ S, A S) ⊑ R
  ; lattice_inf_max   : ∀ (A : L → Prop) R, (∀ S, A S → R ⊑ S) → R ⊑ ∏ S, A S
  }.

Add Parametric Relation (L : Type) (LC : LatticeCore L) (LL : Lattice L) :
    L (@lattice_le L LC)
  reflexivity proved by lattice_le_refl
  transitivity proved by lattice_le_trans
  as lattice_le_rel.

Section LatticeElems.

Context {L : Type} {LC : LatticeCore L} {LL : Lattice L}.

Definition lattice_bot := ∐ _, False.

End LatticeElems.