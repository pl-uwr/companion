Require Import Utf8.
Require Import Setoid.
Require Import Lattice.

Section Progress.
Context {L : Type} {LC : LatticeCore L} {LL : Lattice L}.

Variable progress : L → L → Prop.
Notation "R ↣ S" := (progress R S) (at level 70).

Class Progress : Prop :=
  { progress_monotone_l : ∀ R Q S : L, R ⊑ Q → Q ↣ S → R ↣ S
  ; progress_monotone_r : ∀ R Q S : L, Q ⊑ S → R ↣ Q → R ↣ S
  ; progress_limit_l    : ∀ (A : L → Prop) S,
      (∀ R, A R → R ↣ S) → (∐ R, A R) ↣ S
  }.

Context {PP : Progress}.
Global Instance progress_Proper :
  Morphisms.Proper (lattice_le --> lattice_le ++> Basics.impl) progress.
Proof.
intros R R' HR S S' HS H.
eapply progress_monotone_l; [ eassumption | ].
eapply progress_monotone_r; [ eassumption | ].
assumption.
Qed.

Definition similarity := ∐ R, R ↣ R.

Lemma sim_similarity : similarity ↣ similarity.
Proof.
apply progress_limit_l.
intros R Rsim; eapply progress_monotone_r; [ | eassumption ].
apply lattice_sup_upper; assumption.
Qed.

End Progress.

Arguments progress_monotone_l {L LC progress _}.
Arguments progress_monotone_r {L LC progress _}.
Arguments progress_limit_l    {L LC progress _}.

Section DiProgress.
Context {L : Type} {LC : LatticeCore L} {LL : Lattice L}.

Variable p : L → L → Prop.
Variable b : L → L → Prop.

Context {PP : Progress p} {PB : Progress b}.
Notation "R '↣ₚ' S" := (p R S) (at level 70).
Notation "R '↣ₐ' S" := (b R S) (at level 70).

Definition di_similarity := ∐ R, R ↣ₚ R ∧ R ↣ₐ R.

Lemma di_similarity_p_sim : di_similarity ↣ₚ di_similarity.
Proof.
apply progress_limit_l; intros R HR.
eapply progress_monotone_r; [ | apply HR ].
apply lattice_sup_upper; assumption.
Qed.

Lemma di_similarity_a_sim : di_similarity ↣ₐ di_similarity.
Proof.
apply progress_limit_l; intros R HR.
eapply progress_monotone_r; [ | apply HR ].
apply lattice_sup_upper; assumption.
Qed.

Lemma di_similarity_sim : 
  di_similarity ↣ₚ di_similarity ∧ di_similarity ↣ₐ di_similarity.
Proof. split; [ apply di_similarity_p_sim | apply di_similarity_a_sim ]. Qed.

End DiProgress.